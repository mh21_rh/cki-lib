---
title: cki_lib.footer
linkTitle: footer
description: Generates the common footer for bot activity
---

```bash
usage: python3 -m cki_lib.footer [-h] [--format {text,jira,gitlab}] [--what {created,updated}]

Generate the common footer for bot activity

options:
  -h, --help            show this help message and exit
  --format {text,jira,gitlab}
                        Footer format (default: text)
  --what {created,updated}
                        Whether the footer was created or updated (default: created)ting_reporter
```

## Configuration via environment variables

| Name                 | Type | Secret | Required | Description                                                               |
|----------------------|------|--------|----------|---------------------------------------------------------------------------|
| `FOOTER_CONFIG`      | yaml | no     | no       | Configuration in YAML. If not present, falls back to `FOOTER_CONFIG_PATH` |
| `FOOTER_CONFIG_PATH` | path | no     | no       | Path to the configuration YAML file                                       |
| `SENTRY_DSN`         | url  | yes    | no       | Sentry DSN                                                                |

### Configuring the footer

```yaml
name: cki_lib.foo
source_code: https://gitlab.com/cki-project/cki-lib/cki_lib/foo.py
faq_name: CKI FAQ
faq_url: https://cki-project.org/devel-faq
documentation_url: https://gitlab.com/cki-project/cki-lib/-/blob/main/documenttaion/README.cki_lib.foo.md
slack_name: '#foo'
slack_url: 'https://slack.com/archives/foo'
new_issue_url: 'https://gitlab.com/cki-project/cki-lib/-/issues/new?issue[title]=foo issue'
```
