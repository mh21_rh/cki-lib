"""Add a HashiCorp Vault server to the mix."""
import contextlib
import os
import typing
from unittest import mock

from . import cluster
from .. import misc
from ..logger import get_logger

LOGGER = get_logger(__name__)


class MariaDBServer(cluster.KubernetesCluster):
    """Add a MariaDB server to the mix."""

    @classmethod
    def setUpClass(cls) -> None:
        """Set up the service."""
        super().setUpClass()
        cls.enterClassContext(cls._mariadb())

    @classmethod
    @contextlib.contextmanager
    def _mariadb(cls) -> typing.Iterator[None]:
        service_id = 'mariadb'
        now = misc.now_tz_utc()
        with cls.k8s_namespace(service_id):
            LOGGER.info('Starting MariaDB')
            cls.k8s_deployment(namespace=service_id, name=service_id, setup_at=now, container={
                'image': 'docker.io/library/mariadb:latest',
                'startupProbe': cls.k8s_startup_probe(3306),
                'env': [
                    {'name': 'MARIADB_ROOT_PASSWORD', 'value': 'root'},
                    {'name': 'MARIADB_DATABASE', 'value': 'db'},
                ],
            })
            cls.k8s_service(namespace=service_id, name=service_id)
            if not cls.k8s_wait(namespace=service_id, name=service_id, setup_at=now):
                raise Exception(f'{service_id} did not start up')

            with mock.patch.dict(os.environ, {
                'MARIADB_HOST': cls.hostname,
                'MARIADB_PORT': '3306',
                'MARIADB_USER': 'root',
                'MARIADB_PASSWORD': 'root',
                'MARIADB_DATABASE': 'db',
            }):
                yield
