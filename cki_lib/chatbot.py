"""Send messages to the chatbot."""
import os

from . import messagequeue
from .session import get_session

SESSION = get_session(__name__)


def send_message(message: str) -> None:
    """Send a message to the chatbot."""
    data = {'message': message}
    if chatbot_exchange := os.environ.get('CHATBOT_EXCHANGE'):
        messagequeue.MessageQueue().send_message(
            data,
            'chatbot.message',
            exchange=chatbot_exchange,
            headers={'message-type': 'chatbot'},
        )
    elif chatbot_url := os.environ.get('CHATBOT_URL'):
        SESSION.post(chatbot_url, json=data)
