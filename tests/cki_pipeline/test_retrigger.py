"""Unit tests for cki_pipeline.retrigger()"""
import unittest
from unittest import mock

import responses

from cki_lib import cki_pipeline
from cki_lib.gitlab import get_instance

from . import mocks


class TestRetrigger(unittest.TestCase, mocks.GitLabMocks):
    """Tests for cki_pipeline.retrigger()."""

    variables = {'cki_project': 'cki-project',
                 'cki_pipeline_branch': 'test_branch',
                 'title': 'original-title'}

    def _check_variables(self, *, is_production=True, empty=False):
        api_requests = self.get_requests(url='https://gitlab.com/api/v4/projects/1/pipeline')
        if empty:
            self.assertFalse(api_requests)
            return None
        request_variables = {v['key']: v['value'] for v in api_requests[0]['variables']}
        if is_production:
            self.assertNotIn('CKI_DEPLOYMENT_ENVIRONMENT', request_variables)
        else:
            self.assertIn('CKI_DEPLOYMENT_ENVIRONMENT', request_variables)
        return request_variables

    @responses.activate
    @mock.patch('cki_lib.cki_pipeline._create_custom_configuration',
                mock.Mock(return_value='branch'))
    @mock.patch('cki_lib.cki_pipeline._create_commit', mock.Mock())
    def _check_migrations(self, variables):
        gl_instance = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        self.add_branch(1, self.variables['cki_pipeline_branch'])
        self.add_branch(1, 'branch')
        self.add_gitlab_yml(1)
        gl_project = gl_instance.projects.get(1)
        self.add_pipeline(1, 1, {
            **self.variables,
            **variables,
        })
        cki_pipeline.retrigger(gl_project, 1)
        return self._check_variables(is_production=False)

    def test_variable_migration(self):
        """Check variable migrations."""
        cases = (
            ('officialbuild missing, no brew', {}, {}),
            ('officialbuild missing, implicit scratch', {'brew_task_id': '1'},
             {'officialbuild': 'false'}),
            ('officialbuild missing, explicit scratch', {'brew_task_id': '1', 'scratch': 'true'},
             {'officialbuild': 'false'}),
            ('officialbuild missing', {'brew_task_id': '1', 'scratch': 'false'},
             {'officialbuild': 'true'}),
            ('officialbuild', {'brew_task_id': '1', 'scratch': 'false', 'officialbuild': 'pooh'},
             {'officialbuild': 'pooh'}),
            ('AWS_UPT_LAUNCH_TEMPLATE_NAME', {'AWS_UPT_LAUNCH_TEMPLATE_NAME': 'foo'},
             {}),
            ('kcidb_tree_name missing, no name', {}, {}),
            ('kcidb_tree_name missing', {'name': 'pooh'}, {'kcidb_tree_name': 'pooh'}),
            ('kcidb_tree_name existing, no name', {'kcidb_tree_name': 'pooh'},
             {'kcidb_tree_name': 'pooh'}),
            ('kcidb_tree_name existing, name', {'kcidb_tree_name': 'pooh', 'name': 'bear'},
             {'kcidb_tree_name': 'pooh', 'name': 'bear'}),
        )
        for description, before, after in cases:
            with self.subTest(description):
                variables = self._check_migrations(before)
                for key, value in after.items():
                    if value is None:
                        self.assertNotIn(key, variables)
                    else:
                        self.assertEqual(variables[key], value)
